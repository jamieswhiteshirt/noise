#ifndef PERLIN_H
#define PERLIN_H

#include <random>
#include <sampler.h>

namespace noise
{
	class Random
	{
	private:
		unsigned int _seed;
		std::mersenne_twister_engine<unsigned int, 32, 624, 397, 31, 0x9908b0df, 11, 0xffffffff, 7, 0x9d2c5680, 15, 0xefc60000, 18, 1812433253> _randomEngine;
	public:
		template<typename datatype>
		struct Interval
		{
			datatype min, max;
			Interval(datatype min, datatype max);
		};

		Random(unsigned int seed);

		unsigned int operator()(Interval<unsigned int> interval);
		unsigned int getUint(unsigned int max);
		unsigned int getUint(unsigned int min, unsigned int max);
		unsigned int getUint();
		
		float operator()(Interval<float> interval);
		float getFloat(float max);
		float getFloat(float min, float max);
		float getFloat();
		
		double operator()(Interval<double> interval);
		double getDouble(double max);
		double getDouble(double min, double max);
		double getDouble();
	};

	class PerlinBase
	{
	private:
		void _initNoise(unsigned int mapSize, Random& rand);

	protected:
		const unsigned int _size;
		const unsigned int _octaves;
		unsigned char* _noiseMap;

		PerlinBase(unsigned int dimensions, unsigned int sizeExponent, unsigned int octaves, unsigned int seed);
		PerlinBase(unsigned int dimensions, unsigned int sizeExponent, unsigned int octaves, Random& rand);
		~PerlinBase();
	};

	class Perlin1D : public PerlinBase
	{
	private:
		utility::Sampler1D<unsigned char> _sampler;

	public:
		Perlin1D(unsigned int sizeExponent, unsigned int octaves, unsigned int seed);
		Perlin1D(unsigned int sizeExponent, unsigned int octaves, Random& rand);
		~Perlin1D();

		float operator()(float x, float smoothness = 2.0f);
	};

	class Perlin2D : public PerlinBase
	{
	private:
		utility::Sampler2D<unsigned char> _sampler;

	public:
		Perlin2D(unsigned int sizeExponent, unsigned int octaves, unsigned int seed);
		Perlin2D(unsigned int sizeExponent, unsigned int octaves, Random& rand);
		~Perlin2D();

		float operator()(float x, float y, float smoothness = 2.0f);
	};

	class Perlin3D : public PerlinBase
	{
	private:
		utility::Sampler3D<unsigned char> _sampler;

	public:
		Perlin3D(unsigned int sizeExponent, unsigned int octaves, unsigned int seed);
		Perlin3D(unsigned int sizeExponent, unsigned int octaves, Random& rand);
		~Perlin3D();

		float operator()(float x, float y, float z, float smoothness = 2.0f);
	};

	extern Random globalRandom;
}

#endif
