#include "noise.h"
#include <chrono>

namespace noise
{
	namespace
	{
		float quadraticInterpolation(float f)
		{
			return 1.0f - f * f * (3 - f * 2);
		}
	}

	template<typename datatype>
	Random::Interval<datatype>::Interval(datatype min, datatype max)
		: min(min), max(max)
	{}
	template struct Random::Interval<unsigned int>;
	template struct Random::Interval<float>;
	template struct Random::Interval<double>;

	Random::Random(unsigned int seed)
		: _seed(seed)
	{
		_randomEngine.seed(_seed);
	}

	unsigned int Random::operator()(Random::Interval<unsigned int> interval)
	{
		return getUint(interval.min, interval.max);
	}
	unsigned int Random::getUint(unsigned int max)
	{
		return getUint(0, max);
	}
	unsigned int Random::getUint(unsigned int min, unsigned int max)
	{
		std::uniform_int_distribution<unsigned int> distribution(min, max);
		return distribution(_randomEngine);
	}
	unsigned int Random::getUint()
	{
		return _randomEngine();
	}
	
	float Random::operator()(Random::Interval<float> interval)
	{
		return getFloat(interval.min, interval.max);
	}
	float Random::getFloat(float max)
	{
		return getFloat(0.0f, max);
	}
	float Random::getFloat(float min, float max)
	{
		std::uniform_real_distribution<float> distribution(min, max);
		return distribution(_randomEngine);
	}
	float Random::getFloat()
	{
		return getFloat(1.0f);
	}

	double Random::operator()(Random::Interval<double> interval)
	{
		return getDouble(interval.min, interval.max);
	}
	double Random::getDouble(double max)
	{
		return getDouble(0.0, max);
	}
	double Random::getDouble(double min, double max)
	{
		std::uniform_real_distribution<double> distribution(min, max);
		return distribution(_randomEngine);
	}
	double Random::getDouble()
	{
		return getDouble(1.0);
	}

	void PerlinBase::_initNoise(unsigned int mapSize, Random& rand)
	{
		_noiseMap = new unsigned char[mapSize];

		const int chunk_size = sizeof(unsigned int) * 8;
		for(unsigned int i = 0; i < mapSize; i += chunk_size)
		{
			unsigned int r = rand.getUint();
			for(unsigned int j = 0; j < chunk_size && i + j < mapSize; j++)
			{
				_noiseMap[i + j] = r & 1;
				r >>= 1;
			}
		}
	}
	PerlinBase::PerlinBase(unsigned int dimensions, unsigned int sizeExponent, unsigned int octaves, unsigned int seed)
		: _size(1 << sizeExponent), _octaves(octaves)
	{
		Random rand(seed);
		_initNoise(1 << (sizeExponent * dimensions), rand);
	}
	PerlinBase::PerlinBase(unsigned int dimensions, unsigned int sizeExponent, unsigned int octaves, Random& rand)
		: _size(1 << sizeExponent), _octaves(octaves)
	{
		_initNoise(1 << (sizeExponent * dimensions), rand);
	}
	PerlinBase::~PerlinBase()
	{
		delete[] _noiseMap;
	}

	Perlin1D::Perlin1D(unsigned int sizeExponent, unsigned int octaves, unsigned int seed)
		: PerlinBase(1, sizeExponent, octaves, seed), _sampler(_noiseMap, true, utility::curve::cubic, _size)
	{}
	Perlin1D::Perlin1D(unsigned int sizeExponent, unsigned int octaves, Random& rand)
		: PerlinBase(1, sizeExponent, octaves, rand), _sampler(_noiseMap, true, utility::curve::cubic, _size)
	{}
	Perlin1D::~Perlin1D()
	{}
	float Perlin1D::operator()(float x, float smoothness)
	{
		float noise = 0.0f;
		float value = 1.0f;
		float maxValue = 0.0f;

		for(unsigned int i = 0; i < _octaves; i++)
		{
			maxValue += value;

			noise += _sampler(x) * value;

			x *= 0.5f;
			value *= smoothness;
		}

		return noise / maxValue;
	}

	Perlin2D::Perlin2D(unsigned int sizeExponent, unsigned int octaves, unsigned int seed)
		: PerlinBase(2, sizeExponent, octaves, seed), _sampler(_noiseMap, true, utility::curve::cubic, _size, _size)
	{}
	Perlin2D::Perlin2D(unsigned int sizeExponent, unsigned int octaves, Random& rand)
		: PerlinBase(2, sizeExponent, octaves, rand), _sampler(_noiseMap, true, utility::curve::cubic, _size, _size)
	{}
	Perlin2D::~Perlin2D()
	{}
	float Perlin2D::operator()(float x, float y, float smoothness)
	{
		float noise = 0.0f;
		float value = 1.0f;
		float maxValue = 0.0f;

		for(unsigned int i = 0; i < _octaves; i++)
		{
			maxValue += value;

			noise += _sampler(x, y) * value;

			x *= 0.5f;
			y *= 0.5f;
			value *= smoothness;
		}

		return noise / maxValue;
	}

	Perlin3D::Perlin3D(unsigned int sizeExponent, unsigned int octaves, unsigned int seed)
		: PerlinBase(3, sizeExponent, octaves, seed), _sampler(_noiseMap, true, utility::curve::cubic, _size, _size, _size)
	{}
	Perlin3D::Perlin3D(unsigned int sizeExponent, unsigned int octaves, Random& rand)
		: PerlinBase(3, sizeExponent, octaves, rand), _sampler(_noiseMap, true, utility::curve::cubic, _size, _size, _size)
	{}
	Perlin3D::~Perlin3D()
	{}
	float Perlin3D::operator()(float x, float y, float z, float smoothness)
	{
		float noise = 0.0f;
		float value = 1.0f;
		float maxValue = 0.0f;

		for(unsigned int i = 0; i < _octaves; i++)
		{
			maxValue += value;

			noise += _sampler(x, y, z) * value;

			x *= 0.5f;
			y *= 0.5f;
			z *= 0.5f;
			value *= smoothness;
		}

		return noise / maxValue;
	}

	Random globalRandom = std::chrono::time_point_cast<std::chrono::seconds>(std::chrono::system_clock::now()).time_since_epoch().count();
}
